# code voor een OV-chipkaart paal
import sys
import getopt
import json

version = "1.0"
filename = "input.json"


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


class Error(Exception):
    NOT_AN_OVCARD = "Geen OV-chipkaart."
    NOT_A_VALID_OVCARD = "Ongeldige kaart."
    ALREADY_CHECKEDIN = "Al ingecheckt."

    def __init__(self, num, msg):
        self.num = num
        self.msg = msg


class Status:
    CHECKEDIN = 1
    CHECKEDOUT = 2


def version():
    print("Versie: ", version)


def usage():
    print("Gebruik: ov1.py [opties]")
    print("[opties] is een van:")
    print("  -h, --help                 print deze hulp.")
    print("  -v                         print versie.")
    print("  -f, --file=<filenaam>      lees file in plaats van \"", filename, "\"")


# source of the code for function luhn(n):  http://rosettacode.org/wiki/Luhn_test_of_credit_card_numbers#Python
def luhn(n):
    r = [int(ch) for ch in str(n)][::-1]
    return (sum(r[0::2]) + sum(sum(divmod(d * 2, 10)) for d in r[1::2])) % 10 == 0


def is_readable_ovc(card_number):
    """
    Bepaalt of een kaart een OV-chipkaart is.
    :param card_number: nummer van een kaart.
    :return: True als de kaart een OV-chipkaart is, anders False.

    Deze functie berekent op dezelfde manier als credit cards worden gecontroleerd, namelijk door de
    zgn. mod10-berekening. In deze functie gebeurt dat door de aanroep van luhn().
    """
    return luhn(card_number)


def is_valid_ovc(card_number):
    """
    :param card_number: nummer van een kaart
    :return: True als de OV-chipkaart valide is, anders False
    """
    valid_cards = ["4935910569867379", "4259030189095943", "4532738946206657", "4699985871466702", "4879746637279660",
                   "4149900064887263"]
    if (valid_cards.count(card_number) > 0): return True;
    return False


def is_status_ovc(card_number, status):
    """
    Controleert of de OV-chipkaart een bepaalde status heeft
    :param card_number: nummer van een kaart
    :param status: de status waarop gecheckt moet worden.
    :return: True als de OV-chipkaart die status heeft, anders False.

    Momenteel is alleen geimplementeerd om te controleren of een kaart ingecheckt is.
    """

    checked_in_cards = ["4259030189095943", "4879746637279660"]
    if (status == Status.CHECKEDIN and checked_in_cards.count(card_number) > 0): return True;
    return False


def read_inputfile(filename):
    """
    Leest een JSON-bestand in.
    :param filename:
    :return: een lijst met objecten.
    """
    f = open(filename, 'r')
    list = json.load(f)
    return list


def main(argv=None):
    if argv is None:
        argv = sys.argv
        try:
            try:
                opts, args = getopt.getopt(argv[1:], "hv", ["help"])
                if (len(args) > 1):
                    raise Usage("Te veel argumenten.")
                if (len(args) == 1):
                    filename = args[0]
            except getopt.GetoptError as msg:
                raise Usage(msg)
                # more code, unchanged
        except Usage as err:
            print(err.msg)
            print("Voor help, gebruik --help")
            return 2
        for o, a in opts:
            if o == "-v":
                version()
                sys.exit()
            elif o in ("-h", "--help"):
                usage()
                sys.exit()
            else:
                assert False, "onbekende optie"
        list = read_inputfile(filename)

        for num, card in list:
             try:
                 if (not is_valid_ovc(card)):
                     raise Error(num, Error.NOT_AN_OVCARD)
                 if (is_status_ovc(card, Status.CHECKEDIN)):
                     raise Error(num, Error.NOT_AN_OVCARD)
                 if (not is_readable_ovc(card)):
                     raise Error(num, Error.NOT_AN_OVCARD)
                 raise Error(num, Error.NOT_AN_OVCARD)
             except Error as err:
                print("kaart ", num, ": Fout: ", err.msg)


if __name__ == "__main__":
    sys.exit(main())
